import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, Keyboard, TextInput, TouchableHighlight, NetInfo, AsyncStorage } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5'

import Item from './src/Item'

export default class Todo extends Component {

  constructor(props) {
    super(props);
    this.state = {
      lista:[],
      input:'',
      netStatus:0,
      netStatusColor:'#FF0000'
    }

    this.url = 'https://b7web.com.br/todo/17024';

    this.loadLista = this.loadLista.bind(this);
    this.addButton = this.addButton.bind(this);
    this.conEvent = this.conEvent.bind(this);
    this.sincronizar = this.sincronizar.bind(this);
    this.excluir = this.excluir.bind(this);
    this.atualizar = this.atualizar.bind(this);

    NetInfo.addEventListener('connectionChange', this.conEvent);

    this.loadLista();
  }

  conEvent(info) {
    let state = this.state;
    if(info.type == 'none'){
      state.netStatus = 0;
      state.netStatusColor = '#FF0000';
    } else {
      state.netStatus = 1;
      state.netStatusColor = '#00FF00';
      this.sincronizar();
    }
    this.setState(state);

    if(state.lista.length == 0) {
      this.loadLista();
    }
  }

  loadLista(){
    if(this.state.netStatus == 1) {
      fetch(this.url)
      .then((r) => r.json())
      .then((json) => {
        let state = this.state;
        state.lista = json.todo;
        this.setState(state);

        let lista = JSON.stringify(json.todo);
        AsyncStorage.setItem('lista', lista);
      });
    } else {
      AsyncStorage.getItem('lista')
        .then((v)=>{
          let state = this.state;
          if(v != '') {
            let listaJSON = JSON.parse(v);
            state.lista = listaJSON;
          }
          this.setState(state);
        })
    }
    
  }

  addButton(){
    let texto = this.state.input;

    AsyncStorage.getItem('lista')
        .then((v)=>{
          let state = this.state;
          let listaJSON = JSON.parse(v);
          listaJSON.push({
            item:texto,
            done:'0',
            id:'0'
          });
          state.lista = listaJSON;
          let listaStr = JSON.stringify(listaJSON);
          AsyncStorage.setItem('lista', listaStr);
          state.input = '';
          this.setState(state);
        });
    Keyboard.dismiss();
  }

  sincronizar() {

    if(this.state.netStatus == 1) {
      AsyncStorage.getItem('lista')
      .then((v)=>{
        fetch(this.url+'/sync', {
          method:'POST',
          headers:{
            'Accept':'application/json',
            'Content-Type':'application/json'
          },
          body:JSON.stringify({
            json:v
          })
        })
          .then((r) => r.json())
          .then((json)=>{
            if(json.todo.status) {
              this.loadLista();
              alert('Sincronizado')
            } else {
              alert('Tenta mais tarde')
            }
          })
      });
    } else {
      alert("Você está offline")
    }
  }

  excluir(id) {
    AsyncStorage.getItem('lista')
        .then((v)=>{
          let state = this.state;
          let listaJSON = JSON.parse(v);
          for(var i in listaJSON) {
            if(listaJSON[i].id == id) {
              listaJSON.splice(i, 1);
            }
          }

          state.lista = listaJSON;

          let listaStr = JSON.stringify(listaJSON);
          AsyncStorage.setItem('lista', listaStr);
          
          this.setState(state);
        });
  }

  atualizar(id, done) {
    AsyncStorage.getItem('lista')
        .then((v)=>{
          let state = this.state;
          let listaJSON = JSON.parse(v);

          for(var i in listaJSON) {
            if(listaJSON[i].id == id) {
              if (done == 'sim') {
                listaJSON[i].done = '1';
              } else {
                  listaJSON[i].done = '0';
              }  
            }
          }

          state.lista = listaJSON;

          let listaStr = JSON.stringify(listaJSON);
          AsyncStorage.setItem('lista', listaStr);
          
          this.setState(state);
        });
  }

  render() {
    return(
      <View style={styles.container}>
        <View style={styles.addArea}>
          <Text style={styles.addTxt}>Adicione uma nova tarefa</Text>
          <TextInput style={styles.input} onChangeText={(text) => {
            let state = this.state;
            state.input = text;
            this.setState(state);
          }} value={this.state.input} />
          <TouchableHighlight underlayColor="#CCC" style={styles.button} onPress={this.addButton}>
            <Text style={styles.txtInput}>Adicionar</Text>
          </TouchableHighlight>
        </View>
        <FlatList
          refreshing={false}
          onRefresh={this.sincronizar}
          data={this.state.lista}
          keyExtractor={(item, index) => item.id}
          renderItem={({item}) => <Item data={item} url={this.url} loadFunction={this.loadLista} onDelete={this.excluir} onUpdate={this.atualizar} />}
        />
        <View style={styles.status}>
          <Icon name='wifi' size={24} color={this.state.netStatusColor} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1
  },
  addArea:{
    backgroundColor:'#01A9DB',
    alignItems:'center'
  },
  addTxt:{
    fontSize:20,
    textAlign:'center',
    marginTop:20,
    fontWeight:'bold'
  },
  input:{
    height:40,
    width:350,
    margin:20,
    paddingLeft:10,
    paddingRight:10,
    backgroundColor:'#FFF',
    borderWidth:0.3,
    borderRadius:5
  },
  button:{
    height:40,
    width:100,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'#01DF3A',
    marginBottom:20,
    borderRadius:5
  },
  txtInput:{
    color:'#FFF'
  },
  status:{
    height:50,
    backgroundColor:'#EEE',
    justifyContent:'center',
    alignItems:'center'
  }
});