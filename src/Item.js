import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableHighlight } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5'

export default class Item extends Component {

    constructor(props){
        super(props);
        this.state = {
            icon:(this.props.data.done == '1') ? "check" : "times",
            done:(this.props.data.done == '1') ? "#00FF00" : "#FF0000"
        }

        this.marcar = this.marcar.bind(this);
        this.excluir = this.excluir.bind(this);
    }

    marcar(){
        let state = this.state;

        let done = 'sim';

        if(this.state.done == "#FF0000") {
            state.icon = "check";
            state.done = "#00FF00";
            done = 'sim';
        } else {
            state.icon = "times"
            state.done = "#FF0000";
            done = 'nao';
        }

        this.props.onUpdate(this.props.data.id, done);

        this.setState(state);
    }

    excluir() {
        this.props.onDelete(this.props.data.id);
    }

    render() {
        return(
            <View style={styles.container}>
                
                <View style={{ flexDirection:'row', alignItems:'center', justifyContent:'center' }}>
                    <TouchableHighlight underlayColor="transparent" style={[styles.marcarArea, this.state.done]} onPress={this.marcar}>
                        <Icon name={this.state.icon} size={36} color={this.state.done} />
                    </TouchableHighlight>
                    <Text numberOfLines={1} style={styles.item}>{this.props.data.item}</Text>
                </View>
                <TouchableHighlight underlayColor="transparent" style={[styles.marcarArea, {marginTop:7}]} onPress={this.excluir}>
                        <Icon name="trash-alt" size={28} color="#CCC" />
                </TouchableHighlight>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        paddingTop:10,
        paddingBottom:10,
        borderBottomWidth:1,
        borderColor:'#CCC',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    marcarArea:{
        height:40,
        width:40,
        marginLeft:10
    },
    item:{
        marginLeft:5,
        fontSize:18,
        maxWidth:200
    }
});